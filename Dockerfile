ARG NODE_IMAGE=node:18.20.2
ARG PORT=3000

FROM $NODE_IMAGE AS base
ARG COMMIT_SHA
ENV PATH=./:$PATH
ENV COMMIT_SHA=$COMMIT_SHA
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm i --production

FROM base AS dev
WORKDIR /app
RUN npm i
COPY ./dev ./dev
RUN ./dev installDevOnlyDependencies
COPY . .

FROM base AS build
WORKDIR /app
ENV NODE_ENV=production
COPY . .
RUN npm run build

FROM $NODE_IMAGE AS web
WORKDIR /app
COPY --from=build /app/node_modules ./node_modules
COPY --from=build /app/package*.json ./
COPY --from=build /app/public ./public
COPY --from=build /app/.next ./.next
COPY --from=build /app/healthcheck ./healthcheck
EXPOSE $PORT
CMD ["npm", "run", "start"]
HEALTHCHECK --interval=15s --timeout=3s --start-period=5s --retries=2 CMD ./healthcheck http://localhost:$PORT
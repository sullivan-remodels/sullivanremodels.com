import { metadataForServicePage } from "../components/metadata/metadataForServicePage";
import { canonicalSiteUrl } from "../config/domain";

const url = `${canonicalSiteUrl}/termsAndConditions`;
const title = "Terms and Conditions @ Sullivan Remodels";
const description =
  "Terms and Conditions - Sullivan Remodels";

export const metadata = metadataForServicePage(url, title, description);

export default function TermsAndConditions(){

	return (
		<>
			<div id="main" className="darkTextLightBG">
				<p className="section-header">Sullivan Remodels Terms of Service</p>
				<p>
					These Website Standard Terms and Conditions written on this webpage shall manage 
					your use of our website, Sullivan Remodels accessible at www.sullivsnremodels.com.
					These Terms will be applied fully and affect your use of this Website. By using this 
					Website, you agreed to accept all terms and conditions written in here. You must not 
					use this Website if you disagree with any of these Website Standard Terms and Conditions.
				</p>
				<p>Minors or people below 18 years old are not allowed to use this Website.</p>
				<h3>Intellectual Property Rights</h3>
				<p>
					Other than the content you own, under these Terms, Sullivan Remodels and/or its licensors own 
					all the intellectual property rights and materials contained in this Website. You are granted 
					limited license only for purposes of viewing the material contained on this Website.
				</p>
				<h3>Restrictions</h3>
				<p><strong>You are specifically restricted from all of the following:</strong></p>
					<p>(a) publishing any Website material in any other media, (b) selling, sublicensing and/or otherwise commercializing any 
						Website material, (c) publicly performing and/or showing any Website material, (d) using this Website in any way that 
						is or may be damaging to this Website, (e) using this Website in any way that impacts user access to this Website, 
						(f) using this Website contrary to applicable laws and regulations, or in any way may cause harm to the Website, or to 
						any person or business entity, (g) engaging in any data mining, data harvesting, data extracting or any other similar 
						activity in relation to this Website, or (h) using this Website to engage in any advertising or marketing</p>
				<p>
					Certain areas of this Website are restricted from being access by you and Sullivan Remodels may further restrict access by you to any areas 
					of this Website, at any time, in absolute discretion. Any user ID and password you may have for this Website are confidential and you 
					must maintain confidentiality as well.
				</p>
				<h3>Your Content</h3>
				<p>
					In these Website Standard Terms and Conditions, “Your Content” shall mean any audio, video text, images or other material you choose to 
					display on this Website. By displaying Your Content, you grant Sullivan Remodels a non-exclusive, worldwide irrevocable, sub licensable license 
					to use, reproduce, adapt, publish, translate and distribute it in any and all media. Your Content must be your own and must not be invading 
					any third-party&apos;s rights. Sullivan Remodels reserves the right to remove any of Your Content from this Website at any time without notice.
				</p>
				<h3>No Warranties</h3>
				<p>
					This Website is provided “as is,” with all faults, and Sullivan Remodels expresses no representations or warranties, of any kind related to this 
					Website or the materials contained on this Website. Also, nothing contained on this Website shall be interpreted as advising you.
				</p>
				<h3>Limitation of Liability</h3>
				<p>
					In no event shall Sullivan Remodels, nor any of its officers, directors and employees, shall be held liable for anything arising out of or in any way 
					connected with your use of this Website whether such liability is under contract.  Sullivan Remodels, including its officers, directors and employees 
					shall not be held liable for any indirect, consequential or special liability arising out of or in any way related to your use of this Website.
				</p>
				<h3>Indemnification</h3>
				<p>
					You hereby indemnify to the fullest extent Sullivan Remodels from and against any and/or all liabilities, costs, demands, causes of action, damages 
					and expenses arising in any way related to your breach of any of the provisions of these Terms.
				</p>
				<h3>Severability</h3>
				<p>
					If any provision of these Terms is found to be invalid under any applicable law, such provisions shall be deleted without affecting the 
					remaining provisions herein.
				</p>
				<h3>Variation of Terms</h3>
				<p>
					Sullivan Remodels is permitted to revise these Terms at any time as it sees fit, and by using this Website you are expected to review these 
					Terms on a regular basis.
				</p>
				<h3>Assignment</h3>
				<p>
					The Sullivan Remodels is allowed to assign, transfer, and subcontract its rights and/or obligations under these Terms without any notification. 
					However, you are not allowed to assign, transfer, or subcontract any of your rights and/or obligations under these Terms.
				</p>
				<h3>Entire Agreement</h3>
				<p>
					These Terms constitute the entire agreement between Sullivan Remodels and you in relation to your use of this Website, and supersede all prior 
					agreements and understandings. These Terms will be governed by and interpreted in accordance with the laws of the State of Country, and 
					you submit to the non-exclusive jurisdiction of the state and federal courts located in Country for the resolution of any disputes.
				</p>
			</div>
		</>
	)
}
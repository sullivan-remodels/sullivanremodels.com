import Link from "next/link";
import { FC } from "react";

export const RemodelingTypeButton:FC<{href:string, imageSrc:string, title:string}> = ({href, imageSrc, title}) => (
    <Link href={href}>
      <div className="remodeling-type-button">
        <img
          src={imageSrc}
          alt={title}
          title={title}
        />
        <h1>{title}</h1>
      </div>
    </Link>
  );
import Link from "next/link";
import { navMenu } from "./navMenu";

export const MobileNavMenu = () => (
    <div id="mobile-menu" className="not-displayed" style={{zIndex: 2}}>
      <div style={{ display: "block" }}>
        <div className="mobile-button" id="clear">
          <img src="/Images/backIcon.png" alt="back" />
        </div>
      </div>
      <img id="mobile-logo" src="/Images/mobileMenuLogo.png" alt="Sullivan Remodels Logo" />
      <ul id="mobile-links">
        {navMenu.map(({ name, path }, index, { length }) => (
          <Link
            className={index == 0 ? "first" : index == length - 1 ? "last" : ""}
            href={path}
            key={path}
          >
            <li>{name}</li>
          </Link>
        ))}
      </ul>
    </div>
  );
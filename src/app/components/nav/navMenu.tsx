import { jobberClientLogingUrl } from "../../config/jobber";
import { path as atticPath } from "../../remodeling/attic/path";
import { path as basementPath } from "../../remodeling/basement/path";
import { path as bathroomPath } from "../../remodeling/bathroom/path";
import { path as homeOfficePath } from "../../remodeling/homeOffice/path";
import { path as kitchenPath } from "../../remodeling/kitchen/path";

export const navMenu = [
  { name: "Kitchen", path: kitchenPath},
  { name: "Bath", path: bathroomPath},
  { name: "Basement", path: basementPath},
  { name: "Attic", path: atticPath},
  { name: "Home Office", path: homeOfficePath},
  { name: "Remodeling", path: "/remodeling"},
  { name: "Reviews", path: "/ourReviews" },
  { name: "Login", path: jobberClientLogingUrl }
];

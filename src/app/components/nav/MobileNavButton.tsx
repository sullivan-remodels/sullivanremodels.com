"use client";

import { useEffect } from "react";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const $ = (id: string) => document.getElementById(id) as any;

let shouldShowNav = false;

//NOTE: Sorry this file is hacky, its a step above the separate plain js script file that was buggy in loading times
const toggleNav = () => {
  shouldShowNav = !shouldShowNav;
  const menu = $("mobile-menu");
  const nav = $("mobile-nav");
  if (shouldShowNav) {
    menu.classList.add("displayed");
    menu.classList.remove("not-displayed");
    nav.classList.add("not-displayed");
    nav.classList.remove("displayed");
  } else {
    menu.classList.add("not-displayed");
    menu.classList.remove("displayed");
    nav.classList.add("displayed");
    nav.classList.remove("not-displayed");
  }
};

export const MobileNavButton = () => {
  useEffect(() => {
    shouldShowNav = false;
    $("hamburger").onclick = toggleNav;
    $("clear").onclick = toggleNav;
    $("mobile-links").onclick = toggleNav;
  });
  return (
    <div id="mobile-nav">
      <div className="mobile-button" id="hamburger">
        <img src="/Images/burgerIcon.png" />
      </div>
    </div>
  );
};

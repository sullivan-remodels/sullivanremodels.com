import { ServiceData } from "../metadata/ServiceData";
import { remodelingServices } from "../../remodeling/remodelingServices";
import { RemodelingTypeButton } from "./RemodelingTypeButton";

const remodelingTypeButtonFromServiceData = ({
  path,
  shortName,
  primaryImage,
}: ServiceData) => (
  <RemodelingTypeButton
    href={path}
    imageSrc={primaryImage}
    title={shortName}
    key={path}
  />
);

export const RemodelingTypes = () => (
  <div className="image-grid">
    {remodelingServices.map(remodelingTypeButtonFromServiceData)}
    <RemodelingTypeButton
      href="/remodeling"
      imageSrc="/Images/remodelHero.png"
      title="General Remodeling"
    />
  </div>
);

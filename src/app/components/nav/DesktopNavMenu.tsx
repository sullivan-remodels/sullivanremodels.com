import Link from "next/link";
import { navMenu } from "./navMenu";

    export const DesktopNavMenu = () => (
    <div className="darkTextDarkBG">
      {navMenu.map(({ name, path }, i, { length }) => (
        <>
          <Link href={path}>{name}</Link>
          {i < length - 1 ? " | " : ""}
        </>
      ))}
    </div>
  );
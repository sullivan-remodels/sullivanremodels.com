import { PhotoBase } from "../../photos/Photo";

export const withDecentScore = ({ score }: PhotoBase) => score > 3;

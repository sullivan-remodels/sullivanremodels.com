import Link from "next/link";
import { BeforeAndAfterPhoto } from "../../photos/Photo";

export const BeforeAndAfterRow = ({
  beforeImage,
  afterImage,
  room,
  index,
}: BeforeAndAfterPhoto & { index: number }) => (
  <>
    <div className="photo-grid-item">
      <Link
        href={beforeImage}
        data-lightbox="gallery"
        data-title={`${room} ${index + 1} before`}
      >
        <img src={beforeImage} alt="before" loading="lazy" />
        <h1>Before</h1>
      </Link>
    </div>
    <div className="photo-grid-item">
      <Link
        href={afterImage}
        data-lightbox="gallery"
        data-title={`${room} ${index + 1} after`}
      >
        <img src={afterImage} alt="after" loading="lazy" />
        <h1>After</h1>
      </Link>
    </div>
  </>
);

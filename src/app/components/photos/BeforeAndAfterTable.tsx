import { FC, PropsWithChildren } from "react";
import { BeforeAndAfterPhoto } from "../../photos/Photo";
import { Room } from "../../photos/Room";
import { sortedPhotos } from "../../photos/sortedPhotos";
import { BeforeAndAfterRow } from "./BeforeAndAfterRow";

export const BeforeAndAfterTable: FC<
  PropsWithChildren<{
    description: string;
    photos: BeforeAndAfterPhoto[];
    room: Room;
  }>
> = ({ children, description, room, photos }) => (
  <>
    <p className="section-header">Before &amp; After Photos</p>
    <p>{description}</p>
    <ResponsiveGrid>
      {sortedPhotos(room)(photos).map((p, index) => (
        <BeforeAndAfterRow {...p} index={index} key={p.beforeImage} />
      ))}
      {children}
    </ResponsiveGrid>
  </>
);

export const ResponsiveGrid: FC<PropsWithChildren> = ({ children }) => (
  <div className="image-grid">{children}</div>
);

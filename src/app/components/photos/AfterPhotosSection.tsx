import Link from "next/link";
import { FC } from "react";
import { AfterPhoto } from "../../photos/Photo";
import { Room } from "../../photos/Room";
import { sortedPhotos } from "../../photos/sortedPhotos";

export const AfterPhotosSection: FC<{
  room: Room;
  afterPhotos: AfterPhoto[];
}> = ({ room, afterPhotos }) => (
  <>
    <div style={{ textAlign: "center" }}>
      <h1 className="section-header">More After Photos</h1>
      <small>These are all complete remodels without before pics</small>
    </div>

    <div className="image-grid" style={{ marginTop: "1em" }}>
      {sortedPhotos(room)(afterPhotos).map(({ href }, i) => (
        <div className="photo-grid-item" key={href}>
          <Link href={href} data-lightbox="gallery" data-title={`${room} ${i + 1}`}>
            <img src={href} style={{ width: "100%" }} loading="lazy" />
          </Link>
        </div>
      ))}
    </div>
  </>
);

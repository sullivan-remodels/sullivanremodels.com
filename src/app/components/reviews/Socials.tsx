import { socials } from "../../config/social/socials";

export const Socials = () => (
  <div className="socials">
    <div className="icons">
      {socials.map(({ iconUrl, title, reviewReadUrl }) => (
        <a key={reviewReadUrl} href={reviewReadUrl} target="_blank">
          <img src={iconUrl} alt={title} title={title} />
        </a>
      ))}
    </div>
  </div>
);

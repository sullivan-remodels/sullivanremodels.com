import { ReviewQuote } from "../../config/social/ReviewQuote";
import { ResponsiveGrid } from "../photos/BeforeAndAfterTable";
import { ReviewQuoteTeaser } from "./ReviewQuoteTeaser";
import { Socials } from "./Socials";

export const ReviewsTeaser = ({
  reviewQuotes,
}: {
  reviewQuotes: ReviewQuote[];
}) => (
  <div className="below-the-fold grayBGSection">
    <p className="section-header">Our Reviews</p>
    <p className="section-subtitle">
      More 5-star reviews than anyone out there!
    </p>
    <div>
      <small>
        Click an icon to <strong>read reviews</strong> on these trusted
        platforms:
      </small>
      <Socials />
      <small>Here&apos;s a quick preview:</small>
    </div>
    <ResponsiveGrid>
      {reviewQuotes.map((rq, i) => <ReviewQuoteTeaser key={i} {...rq} />)}
    </ResponsiveGrid>
  </div>
);

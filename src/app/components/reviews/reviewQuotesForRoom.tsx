import { maxQuotesPerPage } from "./maxQuotesPerPage";
import { sortedQuotes } from "./sortedQuotes";
import { Room } from "../../photos/Room";

export const reviewQuotesForRoom = (room: Room) => {
  const roomSpecificQuotes = sortedQuotes.filter((q) =>
    q.rooms.some((r) => r == room)
  );
  const genericQuotes = sortedQuotes.filter((q) => q.rooms.length == 0);
  return [...roomSpecificQuotes, ...genericQuotes].slice(0, maxQuotesPerPage);
};

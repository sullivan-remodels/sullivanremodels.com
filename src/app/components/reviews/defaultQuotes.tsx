import { maxQuotesPerPage } from "./maxQuotesPerPage";
import { sortedQuotes } from "./sortedQuotes";

export const defaultQuotes = sortedQuotes.slice(0, maxQuotesPerPage);
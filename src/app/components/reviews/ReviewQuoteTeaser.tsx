import Link from "next/link";
import { FC } from "react";
import { ReviewQuote } from "../../config/social/ReviewQuote";

const ellipsifyQuote = (quote: string) => {
  const parts = quote.split(" ");
  const shorterQuote = parts.slice(0, parts.length / 2).join(" ");
  return `${shorterQuote}...`;
};

export const ReviewQuoteTeaser: FC<ReviewQuote> = ({
  reviewable: { reviewReadUrl, iconUrl, title },
  reviewerName,
  quote,
  directReviewUrl,
}) => (
  <Link href={directReviewUrl ?? reviewReadUrl} target="_blank">
    <div className="review">
      <p className="review-text">
        &quot;{ellipsifyQuote(quote)}{" "}
        <span style={{ color: "orange" }}>
          ...read more on{" "}
          <img
            style={{ width: "1em", height: "1em" }}
            src={iconUrl}
            alt={title}
          />
        </span>
        &quot;
      </p>
      <p className="reviewer">{reviewerName}</p>
    </div>
  </Link>
);

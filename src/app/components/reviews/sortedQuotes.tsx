import { reviewQuotes } from "../../config/social/reviewQuotes";


export const sortedQuotes = [...reviewQuotes.sort((a, b) => b.internalScore - a.internalScore)];

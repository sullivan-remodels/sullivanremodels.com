import Link from "next/link";
import { FC } from "react";
import { Reviewable } from "../../config/social/Reviewable";

export const ReviewCreateButtonLarge: FC<{ reviewable: Reviewable }> = ({
  reviewable: { iconUrl, title, reviewCreateUrl },
}) => (
  <Link href={reviewCreateUrl} target="_blank">
    <div className="link-wrapper">
      <div className="link-img-wrapper">
        <img src={iconUrl} alt={title} />
      </div>
      <div className="link-name-wrapper">
        <p>Review Us on {title}</p>
      </div>
    </div>
  </Link>
);

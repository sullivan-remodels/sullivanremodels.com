import { ResponsiveGrid } from "../photos/BeforeAndAfterTable";
import { ReviewCreateButtonLarge } from "./ReviewCreateButtonLarge";
import { socials } from "../../config/social/socials";

export const ReviewUsIcons = () => (
  <ResponsiveGrid>
    {socials.map((r) => (
      <ReviewCreateButtonLarge key={r.iconUrl} reviewable={r} />
    ))}
  </ResponsiveGrid>
);

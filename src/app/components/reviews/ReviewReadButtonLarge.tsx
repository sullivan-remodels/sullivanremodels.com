import Link from "next/link";
import { FC } from "react";
import { Reviewable } from "../../config/social/Reviewable";

export const ReviewReadButtonLarge: FC<{ reviewable: Reviewable }> = ({
  reviewable: { iconUrl, title, reviewReadUrl },
}) => (
  <Link href={reviewReadUrl} target="_blank">
    <div className="link-wrapper">
      <div className="link-img-wrapper">
        <img src={iconUrl} alt={title} />
      </div>
      <div className="link-name-wrapper">
        <p>{title} Reviews</p>
      </div>
    </div>
  </Link>
);

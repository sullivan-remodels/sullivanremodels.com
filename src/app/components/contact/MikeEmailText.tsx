import { FC, PropsWithChildren } from "react";
import { mikeEmailAddress } from "../../config/mikeEmailAddress";
import Link from "next/link";

export const mikeEmailHref = `mailto:${mikeEmailAddress}`

export const MikeEmailLink: FC<PropsWithChildren> = ({ children }) => (
  <Link className="emailMike" href={mikeEmailHref}>{children}</Link>
);

export const MikeEmailText = () => (
  <MikeEmailLink>
    <strong>{mikeEmailAddress}</strong>
  </MikeEmailLink>
);

import { MikeCallLink } from "./MikeCallLink";

export const MikeCallButton = () => <MikeCallLink>
    <input
        id="call-button"
        type="button"
        className="contactButton orange"
        value="Call Us" />
</MikeCallLink>;

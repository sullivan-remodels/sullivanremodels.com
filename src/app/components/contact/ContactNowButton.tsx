import Link from "next/link";
import { path } from "../../contact/path";

export const ContactNowButton = () => (
    <Link href={path}>
      <input type="button" className="contactButton orange" value="Contact / Get Quote" />
    </Link>
  );
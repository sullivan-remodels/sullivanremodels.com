import { JobberLink } from "./JobberLink";

export const JobberContactButton = () => (
  <JobberLink>
    <input
      id="call-button"
      type="button"
      className="contactButton orange"
      value="Request a Quote"
    />
  </JobberLink>
);

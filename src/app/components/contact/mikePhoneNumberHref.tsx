import { mikePhoneNumber } from "../../config/mikePhoneNumber";

export const mikePhoneNumberHref = `tel:${mikePhoneNumber}`;

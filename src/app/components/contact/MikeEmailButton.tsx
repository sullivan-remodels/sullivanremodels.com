import { MikeEmailLink } from "./MikeEmailText";

export const MikeEmailButton = () => (
    <MikeEmailLink>
        <input type="button" id="mikeEmail" className="contactButton orange" value="Email Us" />
    </MikeEmailLink>
);

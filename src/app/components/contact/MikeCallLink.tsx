import { FC, PropsWithChildren } from "react";
import { mikePhoneNumberHref } from "./mikePhoneNumberHref";
import Link from "next/link";
import { mikePhoneNumber } from "../../config/mikePhoneNumber";

export const MikeCallLink: FC<PropsWithChildren> = ({ children }) => <Link href={mikePhoneNumberHref}>{children}</Link>;
export const MikeTextLink: FC<PropsWithChildren> = ({ children }) => <Link href={`sms:${mikePhoneNumber}`}>{children}</Link>;

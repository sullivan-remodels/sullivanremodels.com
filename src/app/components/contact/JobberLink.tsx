import { jobberWorkRequestUrl } from "../../config/jobber";
import { FC, PropsWithChildren } from "react";

export const JobberLink: FC<PropsWithChildren> = ({ children }) => (
  <a href={jobberWorkRequestUrl} target="sullivan-remodels-contact">
    {children}
  </a>
);

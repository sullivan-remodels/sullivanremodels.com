import { FC, PropsWithChildren } from "react";
import { remodelingServices } from "../../remodeling/remodelingServices";
import { itemOfferedForServiceData } from "../metadata/jsonld/itemOfferedForServiceData";
import { offerCatalogWithChildServices } from "../metadata/jsonld/offerCatalogWithChildServices";
import { Page, PageProps } from "./Page";

type ServiceCatalogProps = PropsWithChildren<
  Omit<PageProps, "jsonLD" | "title">
> & {
  description: string;
  heroTitle: string;
  metaTitle: string;
};
export const ServiceCatalogPage: FC<ServiceCatalogProps> = ({
  children,
  metaTitle,
  description,
  heroTitle,
  ...pageProps
}) => {
  const itemsOffered = remodelingServices.map(itemOfferedForServiceData);
  const jsonLD = offerCatalogWithChildServices(
    metaTitle,
    description,
    itemsOffered
  );
  return (
    <Page {...pageProps} {...{ title: heroTitle, jsonLD }}>
      {children}
    </Page>
  );
};

import { FC, PropsWithChildren } from "react";
import { Room } from "../../photos/Room";
import { serviceJSONLD } from "../metadata/jsonld/serviceJSONLD";
import { Page } from "./Page";
import { reviewQuotesForRoom } from "../reviews/reviewQuotesForRoom";

export const ServicePage: FC<
  PropsWithChildren<{
    description: string;
    serviceName: string;
    serviceOfferingNames: string[];
    photoHrefs: string[];
    primaryImage: string;
    subtitle: string;
    room: Room;
  }>
> = ({
  children,
  description,
  serviceName,
  serviceOfferingNames,
  photoHrefs,
  primaryImage,
  subtitle,
  room,
}) => {
  const jsonLD = serviceJSONLD(
    description,
    serviceName,
    serviceOfferingNames,
    photoHrefs
  );
  const reviewQuotes = reviewQuotesForRoom(room);

  return (
    <Page {...{ title: serviceName, subtitle, primaryImage, jsonLD, reviewQuotes }}>
      {children}
    </Page>
  );
};

import { FC, PropsWithChildren } from "react";


export const HeroBase: FC<
  PropsWithChildren<{
    primaryImage: string;
  }>
> = ({ primaryImage, children }) => (
  <div
    className="hero"
    style={{
      backgroundImage: `url(${primaryImage})`,
    }}
  >
    <div className="hero-text">{children}</div>
  </div>
);

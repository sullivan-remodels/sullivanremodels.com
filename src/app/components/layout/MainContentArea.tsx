import { FC, PropsWithChildren } from "react";

export const MainContentArea: FC<PropsWithChildren> = ({ children }) => (
  <div id="main" className="darkTextLightBG">
    {children}
  </div>
);

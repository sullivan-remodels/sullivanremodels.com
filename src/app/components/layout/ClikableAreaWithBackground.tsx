import Link from "next/link";
import { PropsWithChildren } from "react";

export const ClikableAreaWithBackground =
  (style: "left-or-top" | "right-or-bottom") =>
  // eslint-disable-next-line react/display-name
  ({
    href,
    imageHref,
    children,
  }: PropsWithChildren<{ href: string; imageHref: string }>) =>
    (
      <div className={style}>
        <Link href={href}>
          <div
            className="image-button"
            style={{ backgroundImage: `url(${imageHref})` }}
          >
            <div>{children}</div>
          </div>
        </Link>
      </div>
    );

export const LeftOrTopArea = ClikableAreaWithBackground("left-or-top");
export const RightOrBottomArea = ClikableAreaWithBackground("right-or-bottom");

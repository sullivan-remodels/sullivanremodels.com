import { FC, PropsWithChildren } from "react";
import { ReviewQuote } from "../../config/social/ReviewQuote";
import { JSONLD } from "../metadata/jsonld/JSONLD";
import { JSONLDData } from "../metadata/jsonld/JSONLDData";
import { ReviewsTeaser } from "../reviews/ReviewsTeaser";
import { Hero } from "./Hero";
import { MainContentArea } from "./MainContentArea";

export type PageProps = PropsWithChildren<{
  title: string;
  primaryImage: string;
  subtitle: string;
  reviewQuotes: ReviewQuote[];
  jsonLD: JSONLDData;
}>;
export const Page: FC<PageProps> = ({
  children,
  title,
  primaryImage,
  subtitle,
  reviewQuotes,
  jsonLD,
}) => {
  return (
    <>
      <JSONLD {...{ jsonLD }} />
      <Hero {...{ primaryImage, title, subtitle }} />
      <MainContentArea>{children}</MainContentArea>
      <ReviewsTeaser reviewQuotes={reviewQuotes} />
    </>
  );
};

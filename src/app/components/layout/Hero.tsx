import { FC } from "react";
import { ContactButtons } from "../contact/ContactButtons";
import { HeroBase } from "./HeroBase";

export const Hero: FC<{
  primaryImage: string;
  title: string;
  subtitle: string;
}> = ({ primaryImage, title, subtitle }) => (
  <HeroBase {...{ primaryImage }}>
    <h1>{title}</h1>
    <h2>{subtitle}</h2>
    <ContactButtons />
  </HeroBase>
);

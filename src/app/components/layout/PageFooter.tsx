import { path } from "../../contact/path";
import { ContactNowButton } from "../contact/ContactNowButton";
import { JobberLink } from "../contact/JobberLink";
import { MikeCallLink, MikeTextLink } from "../contact/MikeCallLink";
import { MikeEmailLink } from "../contact/MikeEmailText";

export const PageFooter = () => (
  <div className="below-the-fold last-section">
    <p className="section-header">Get a Quote!</p>
    <p>
      Fill our quick <JobberLink>quote request form</JobberLink> for the best
      experience, or <a href={path}>contact us</a> via{" "}
      <MikeTextLink>text</MikeTextLink>, <MikeCallLink>phone</MikeCallLink> or{" "}
      <MikeEmailLink>email</MikeEmailLink>
    </p>
    <div className="svg-grid">
      <img className="svg-item" src="/Images/payment-icons/applePay.svg" />
      <img className="svg-item" src="/Images/payment-icons/gpay.svg" />
      <img className="svg-item" src="/Images/payment-icons/visa.svg" />
      <img className="svg-item" src="/Images/payment-icons/mastercard.svg" />
      <img className="svg-item" src="/Images/payment-icons/amex.svg" />
      <img className="svg-item" src="/Images/payment-icons/discover.svg" />
    </div>
    <p>
      We are fully insured and incorporated to do business in the state of Ohio.
    </p>
    <ContactNowButton />
  </div>
);

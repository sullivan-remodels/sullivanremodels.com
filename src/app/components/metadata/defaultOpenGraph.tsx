import { logoForLinkSharing } from "../../config/logoForLinkSharing";
import { mikeEmailAddress } from "../../config/mikeEmailAddress";

export const defaultOpenGraph = {
  type: "website",
  phoneNumbers: ["440.666.2576"],
  emails: [mikeEmailAddress],
  siteName: "Sullivan Remodels",
  images: [logoForLinkSharing],
  locale: "en_US",
};

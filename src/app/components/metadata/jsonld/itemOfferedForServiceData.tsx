import { canonicalSiteUrl } from "../../../config/domain";
import { ServiceData } from "../ServiceData";
import { ItemOffered } from "./ItemOffered";

export const itemOfferedForServiceData = ({
  description, path, serviceName,
}: ServiceData): ItemOffered => ({
  name: serviceName,
  description,
  url: `${canonicalSiteUrl}${path}`,
});

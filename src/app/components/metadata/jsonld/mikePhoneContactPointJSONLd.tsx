import { mikePhoneNumber } from "../../../config/mikePhoneNumber";

export const mikePhoneContactPointJSONLd = {
  "@type": "ContactPoint",
  "telephone": { mikePhoneNumber },
  "contactType": "Customer Service",
  "areaServed": "US",
  "availableLanguage": ["English"],
};

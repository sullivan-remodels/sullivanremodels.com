import { logoForLinkSharing } from "../../../config/logoForLinkSharing";
import { mikePhoneNumber } from "../../../config/mikePhoneNumber";
import { localBusinessProviderRef } from "./localBusinessProviderRef";
import { placeJSONLD } from "./placeJSONLD";
import { postalAddressJSONLD } from "./postalAddressJSONLD";

export const providerBaseJSONLD = {
  "@context": "https://schema.org",
  ...localBusinessProviderRef,
  address: postalAddressJSONLD,
  telephone: mikePhoneNumber,
  openingHours: "Mo-Fr 09:00-17:00",
  image: logoForLinkSharing,
  serviceArea: placeJSONLD,
};

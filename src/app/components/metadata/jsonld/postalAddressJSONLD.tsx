export const postalAddressJSONLD = {
  "@type": "PostalAddress",
  streetAddress: "4889 Delevan Dr",
  addressLocality: "Lyndhurst",
  addressRegion: "OH",
  postalCode: "44124",
  addressCountry: "US",
};

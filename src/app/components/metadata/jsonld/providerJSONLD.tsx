import { canonicalSiteUrl } from "../../../config/domain";
import { socials } from "../../../config/social/socials";
import { metaTitle } from "../../../remodeling/metaTitle";
import { path } from "../../../remodeling/path";
import { providerBaseJSONLD } from "./providerBaseJSONLD";

export const providerJSONLD = {
  ...providerBaseJSONLD,
  sameAs: [...socials.map((s) => s.profileUrl), "https://www.paintcleveland.com"],
  hasOfferCatalog: {
    "@type": "OfferCatalog",
    name: metaTitle,
    url: `${canonicalSiteUrl}${path}`,
  },
};

export type ProviderJSONLD = typeof providerJSONLD;
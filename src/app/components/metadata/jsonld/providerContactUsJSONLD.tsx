import { jobberWorkRequestUrl } from "../../../config/jobber";
import { providerBaseJSONLD } from "./providerBaseJSONLD";
import { mikePhoneContactPointJSONLd } from "./mikePhoneContactPointJSONLd";
import { mikeEmailAddress } from "../../../config/mikeEmailAddress";

export const providerContactUsJSONLD = {
  ...providerBaseJSONLD,
  "contactPoint": [
    mikePhoneContactPointJSONLd,
    {
      ...mikePhoneContactPointJSONLd,
      "contactOption": "SMS"
    },
    {
      "@type": "ContactPoint",
      "email": { mikeEmailAddress },
      "contactType": "Email Support",
      "areaServed": "Global",
      "availableLanguage": ["English"]
    },
    {
      "@type": "ContactPoint",
      "url": jobberWorkRequestUrl,
      "contactType": "Online Form",
      "description": "Submit a quote request form to begin the quoting and scheduling process."
    }
  ]
};

export type ProviderContactUsJSONLD = typeof providerContactUsJSONLD;

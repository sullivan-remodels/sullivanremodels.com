import { companyName } from "../../../config/companyName";
import { canonicalSiteUrl } from "../../../config/domain";

export const localBusinessProviderRef = {
  "@type": "LocalBusiness",
  name: companyName,
  url: canonicalSiteUrl,
};

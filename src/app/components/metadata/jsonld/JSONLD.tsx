import { FC } from "react";
import { JSONLDData } from "./JSONLDData";
import Script from "next/script";

export const JSONLD: FC<{ jsonLD: JSONLDData; }> = ({ jsonLD }) => (
  <Script
    id="jsonLD"
    strategy="beforeInteractive"
    type="application/ld+json"
    dangerouslySetInnerHTML={{
      __html: JSON.stringify(jsonLD),
    }} />
);

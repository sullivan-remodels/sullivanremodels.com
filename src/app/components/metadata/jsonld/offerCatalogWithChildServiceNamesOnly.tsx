import { itemListElementForJSONLD } from "./itemListElementForJSONLD";

export const offerCatalogWithChildServiceNamesOnly = (
  serviceName: string,
  serviceOfferingNames: string[]
) => ({
  "@type": "OfferCatalog",
  name: `${serviceName} Offerings`,
  itemListElement: itemListElementForJSONLD(
    serviceOfferingNames.map((name) => ({ name }))
  ),
});

import { ItemOffered } from "./ItemOffered";

export const itemListElementForJSONLD = (itemsOffered: ItemOffered[]) => itemsOffered.map(({name, url, description}) => ({
  "@type": "Offer",
  itemOffered: {
    "@type": "Service",
    name,
    ...(url ? { url } : {}),
    ...(description ? { description } : {}),
  },
}));

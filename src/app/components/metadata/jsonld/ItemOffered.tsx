export type ItemOffered = {
  name: string;
  url?: string;
  description?: string;
};

import { itemListElementForJSONLD } from "./itemListElementForJSONLD";
import { ItemOffered } from "./ItemOffered";

export const offerCatalogWithChildServiceRefs = (
  catalogName: string,
  itemsOffered: ItemOffered[]
) => ({
  "@type": "OfferCatalog",
  name: catalogName,
  itemListElement: itemListElementForJSONLD(itemsOffered),
});

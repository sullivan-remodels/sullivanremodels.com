import { ItemOffered } from "./ItemOffered";
import { localBusinessProviderRef } from "./localBusinessProviderRef";
import { offerCatalogWithChildServiceRefs } from "./offerCatalogWithChildServiceRefs";

export const offerCatalogWithChildServices = (catalogName: string, description: string, itemsOffered: ItemOffered[]) => ({
  "@context": "https://schema.org",
  description,
  provider: localBusinessProviderRef,
  ...offerCatalogWithChildServiceRefs(catalogName, itemsOffered),
});

export type OfferCatalogJSONLDWithChildServices = ReturnType<typeof offerCatalogWithChildServices>;
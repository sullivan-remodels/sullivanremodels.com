import { OfferCatalogJSONLDWithChildServices } from "./offerCatalogWithChildServices";
import { ProviderContactUsJSONLD } from "./providerContactUsJSONLD";
import { ProviderJSONLD } from "./providerJSONLD";
import { ServiceJSONLD } from "./serviceJSONLD";

export type JSONLDData = ServiceJSONLD | OfferCatalogJSONLDWithChildServices | ProviderJSONLD | ProviderContactUsJSONLD;


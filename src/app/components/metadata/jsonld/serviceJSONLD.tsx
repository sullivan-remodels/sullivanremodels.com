import { maxPhotosForJSONLD } from "./maxPhotosForJSONLD";
import { canonicalSiteUrl } from "../../../config/domain";
import { localBusinessProviderRef } from "./localBusinessProviderRef";
import { offerCatalogWithChildServiceNamesOnly } from "./offerCatalogWithChildServiceNamesOnly";

export const serviceJSONLD = (
  description: string,
  serviceName: string,
  serviceOfferingNames: string[],
  photosHrefs: string[]
) => ({
  "@context": "https://schema.org",
  "@type": "Service",
  name: serviceName,
  description,
  areaServed: {
    "@type": "Place",
    name: "Lyndhurst, Ohio",
  },
  provider: localBusinessProviderRef,
  hasOfferCatalog: offerCatalogWithChildServiceNamesOnly(
    serviceName,
    serviceOfferingNames
  ),
  image: photosHrefs
    .slice(0, maxPhotosForJSONLD)
    .map((p) => `${canonicalSiteUrl}${p}`),
});

export type ServiceJSONLD = ReturnType<typeof serviceJSONLD>;

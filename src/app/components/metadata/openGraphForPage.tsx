import { canonicalSiteUrl } from "../../config/domain";
import { defaultOpenGraph } from "./defaultOpenGraph";
import { OG } from "./OG";

export const openGraphForPage = (
  url: OG["url"],
  title: OG["title"],
  description: OG["description"],
  primaryImage?: string
): OG => ({
  url,
  title,
  description,
  ...defaultOpenGraph,
  images: primaryImage ? [`${canonicalSiteUrl}${primaryImage}`] : defaultOpenGraph.images,
});

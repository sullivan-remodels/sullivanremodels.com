import { canonicalSiteUrl } from "../../config/domain";
import { openGraphForPage } from "./openGraphForPage";

export const metadataForPage = (
  path: string,
  title: string,
  description: string,
  primaryImage?: string
) => {
  const url = `${canonicalSiteUrl}${path}`;
  return {
    title,
    description,
    openGraph: openGraphForPage(url, title, description, primaryImage),
    alternates: {
      canonical: url,
    },
  };
};


export type ServiceData = {
  shortName: string;
  serviceName: string;
  path: string;
  description: string;
  primaryImage: string;
};

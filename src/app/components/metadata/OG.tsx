import { Metadata } from "next";

export type OG = NonNullable<Metadata["openGraph"]>;
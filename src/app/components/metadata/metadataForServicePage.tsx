import { Metadata } from "next";
import { companyName } from "../../config/companyName";
import { metadataForPage } from "./metadataForPage";

export const metadataForServicePage = (
  path: string,
  serviceName: string,
  description: string,
  pageImage?: string
): Metadata => {
  const title = `${serviceName} @ ${companyName}`;
  return metadataForPage(path, title, description, pageImage);
};

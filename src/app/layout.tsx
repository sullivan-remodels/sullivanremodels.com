import { GoogleTagManager } from "@next/third-parties/google";
import Link from "next/link";
import Script from "next/script";
import { ContactNowButton } from "./components/contact/ContactNowButton";
import { PageFooter } from "./components/layout/PageFooter";
import { metadataForServicePage } from "./components/metadata/metadataForServicePage";
import { DesktopNavMenu } from "./components/nav/DesktopNavMenu";
import { MobileNavButton } from "./components/nav/MobileNavButton";
import { MobileNavMenu } from "./components/nav/MobileNavMenu";
import { description } from "./config/description";
import { canonicalSiteUrl } from "./config/domain";
import { googleTagManagerId } from "./config/social/google";

const title = "Sullivan Remodels";
export const metadata = metadataForServicePage(canonicalSiteUrl, title, description);

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <head>
        <GoogleTagManager gtmId={googleTagManagerId} />
        <meta httpEquiv="Content-Type" content="text/html charset=utf-8" />
        <meta
          name="COMMIT_SHA"
          content={process.env.COMMIT_SHA ?? "unofficial build"}
        />
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.4/css/lightbox.min.css"
        />
        <link rel="stylesheet" type="text/css" href="/Styles/hero.css" />
        <link rel="stylesheet" type="text/css" href="/Styles/main.css" />
        <link rel="stylesheet" type="text/css" href="/Styles/mobileNav.css" />
        <link rel="stylesheet" type="text/css" href="/Styles/responsive.css" />
        <link rel="icon" type="image/png" href="/Images/favicon.png" />
        <meta
          name="google-site-verification"
          content="8jYcykuJP0U92ZcSYr3sZeKeaA4ozZU4d3g9sE-LD9k"
        />
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
        />
        <Script src="https://code.jquery.com/jquery-3.6.0.min.js"></Script>
        <Script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.4/js/lightbox.min.js"></Script>
      </head>
      <body>
        <noscript>
          <iframe
            src={`https://www.googletagmanager.com/ns.html?id=${googleTagManagerId}`}
            height="0"
            width="0"
            style={{ display: "none", visibility: "hidden" }}
          ></iframe>
        </noscript>
        <div id="root">
          <MobileNavMenu />
          <div id="content-div">
            <div>
              <div>
                <Link href="/">
                  <img
                    id="logo"
                    src="/Images/sullivanRemodelsLogo.png"
                    alt="Sullivan Remodels"
                  />
                </Link>
                <div id="contact-div">
                  <ContactNowButton />
                </div>
              </div>
              <DesktopNavMenu />
              <MobileNavButton />
            </div>
            {children}
            <PageFooter />
          </div>
          <div style={{ width: "100%" }}>
            <p style={{ textAlign: "center" }} className="footer">
              Copyright 2024, Sullivan Remodels, LLC. All Rights Reserved.{" "}
            </p>
            <p style={{ textAlign: "center" }} className="footer">
              View our <Link href="/privacyPolicy">Privacy Policy</Link> and{" "}
              <Link href="/termsAndConditions">Terms of Service</Link>
            </p>
            <p style={{ textAlign: "center" }} className="footer">
              <a
                className="attribution"
                href="https://www.flaticon.com/free-icons/instagram-logo"
                title="instagram logo icons"
              >
                Social media logo icons created by Pixel perfect - Flaticon
              </a>
            </p>
            <br />
          </div>
        </div>
      </body>
    </html>
  );
}

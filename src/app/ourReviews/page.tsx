import Link from "next/link";
import { ResponsiveGrid } from "../components/photos/BeforeAndAfterTable";
import { defaultQuotes } from "../components/reviews/defaultQuotes";
import { ReviewQuoteTeaser } from "../components/reviews/ReviewQuoteTeaser";
import { ReviewReadButtonLarge } from "../components/reviews/ReviewReadButtonLarge";
import { socials } from "../config/social/socials";

export default function OurReviews() {
  return (
    <>
      <div
        className="hero"
        style={{ backgroundImage: "url(/Images/reviewHero.png)" }}
      >
        <div className="hero-text">
          <h1>Reviews</h1>
          <h2>Read What Others Have Said</h2>
        </div>
      </div>
      <div id="main" className="darkTextLightBG">
        <h3>
          Click the links below to read our reviews on these trusted platforms
        </h3>
        <ResponsiveGrid>
          {socials.map((r) => (
            <ReviewReadButtonLarge key={r.iconUrl} reviewable={r} />
          ))}
        </ResponsiveGrid>
      </div>
      <div className="below-the-fold last-section">
        <p className="section-header">Check These Out!</p>
        <p className="section-subtitle">Some Reviews We&apos;re Proud Of</p>
        <ResponsiveGrid>
          {defaultQuotes.map((rq, i) => (
            <ReviewQuoteTeaser key={i} {...rq} />
          ))}
        </ResponsiveGrid>
      </div>
      <div className="below-the-fold last-section grayBGSection">
        <p className="section-header">Review Us!</p>
        <p>Share your thoughts with us and with future customers!</p>
        <Link href="reviewUs">
          <input
            type="button"
            className="contactButton orange"
            value="Review Now"
          />
        </Link>
      </div>
    </>
  );
}

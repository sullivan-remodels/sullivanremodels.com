import { angi } from "./angi";
import { facebook } from "./facebook";
import { google } from "./google";
import { ReviewQuote } from "./ReviewQuote";

export const reviewQuotes: ReviewQuote[] = [
  {
    reviewable: google,
    quote: `Mike was fantastic to work with and my basement redo came out
            beautifully. The project looks great, came in on budget and on
            time...what more could I ask for? I will definitely be calling for
            future projects.`,
    reviewerName: "Leah E.",
    rooms: ["basement"],
    internalScore: 7,
    directReviewUrl: "https://maps.app.goo.gl/EwkR87ca5dFoePbdA",
  },
  {
    reviewable: google,
    quote: `Mike did a great paint job in the living and dining rooms of my 1929
            cape cod bungalow. It looks pleasing to the eye and professionally
            orchestrated. Mike was right on target with the initial estimate.`,
    reviewerName: "Kathy M.",
    rooms: ["livingRoom", "diningRoom"],
    internalScore: 3,
    // directReviewUrl:
  },
  {
    reviewable: google,
    quote: `I would highly recommend Mike and his team. We have worked with Mike
            on multiple jobs both large and small and he has gone above and
            beyond each time.`,
    reviewerName: "Matt B.",
    rooms: [],
    internalScore: 1,
    // directReviewUrl:
  },
  {
    reviewable: google,
    quote: `Mike did an amazing job on my redoing my kitchen! I couldn't be
            any happier with the results!! I will definitely use Mike for future
            jobs and would recommend him to everyone.`,
    reviewerName: "Loren B.",
    rooms: ["kitchen"],
    internalScore: 6,
    directReviewUrl: "https://maps.app.goo.gl/21At83wdsxBRjnNm8",
  },
  {
    reviewable: facebook,
    quote: `We had our kitchen redone in September 2024 by Mike and Ralph from Sullivan Remodels. Great people, excellent workmanship, very accommodating and the job was well done. We HIGHLY RECOMMEND Sullivan Remodels.`,
    reviewerName: "DJ (Rich) & Denise",
    rooms: ["kitchen"],
    internalScore: 10,
    directReviewUrl:
      "https://www.facebook.com/rich.novak.735/posts/pfbid02fnLCgkqQqycCCk4Qjpw6xhEwXwsQjw6sjikHar3nMiJJ9BN2y92PRiXVXAga9sKsl",
  },
  {
    reviewable: facebook,
    quote: `We went with Sullivan Remodels for a full kitchen and partial bath remodel and couldn’t be happier. Mike was a pleasure to work with. You can tell he takes pride in his work and pays attention to detail. He sets accurate expectations for pricing and job completion.`,
    reviewerName: "Drew J.",
    rooms: ["kitchen", "bathroom"],
    internalScore: 10,
    directReviewUrl:
      "https://www.facebook.com/ajursich/posts/pfbid0Zs5wSEF2FSRRq3WxPXC6JXPJgw5yvBiB7ck8u3yhzWTiyyNdRd4Rq469VNQ151Dnl",
  },
  {
    reviewable: facebook,
    quote: `Over the last 2 years Mike has helped us update bathroom vanities, paint rooms with huge vaulted ceilings, including a foyer around the staircase. He always does such a beautiful job! I appreciate when he offers ideas and advice on a project too! Thanks, Mike, for always doing an awesome job!`,
    reviewerName: "Janelle O.",
    rooms: ["kitchen", "bathroom"],
    internalScore: 10,
    directReviewUrl:
      "https://www.facebook.com/janell.oloughlin/posts/pfbid0YiaPsZyB2rPJ4T1r4YiT2EsHWrRbhjdJWdppp4yGA61dJA4yt5e1KncdngCccvUAl",
  },
  {
    reviewable: facebook,
    quote: `We first met Mike when he told us he could repaint our kitchen cabinets - a job which several other painters had turned down as impossible.  He did a beautiful lasting job. We  subsequently hired him to completely repaint the condo we purchased, and again, we could not have been happier. His works is great and immaculate and he is wonderful about staying in touch, returning messages and coming when he says he will.`,
    reviewerName: "Judy B.",
    rooms: ["kitchen"],
    internalScore: 5,
    directReviewUrl:
      "https://www.facebook.com/JCBurdsall/posts/pfbid02JrnkPwrreFcJ9XJ1hSd2gJULjB8mQJEPzi3WQem9U5ATcPCZ2X577EZc9rf7Jh1sl",
  },
  {
    reviewable: facebook,
    quote: `Mike is always prompt, thorough and courteous. He's been working for us for years and we recommend him highly.`,
    reviewerName: "Phil S,",
    rooms: [],
    internalScore: 4,
    directReviewUrl:
      "https://www.facebook.com/phil.stella1/posts/pfbid033pRBeaVsZ67WGtKaX1pQ7QgEzpPEajmcTGjQaEd3FjozJgtYULhv6Cgho3J1uzAZl",
  },
  {
    reviewable: facebook,
    quote: `Mike gets the job done & is easy to work with.`,
    reviewerName: "Lynne J.",
    rooms: [],
    internalScore: 2,
    directReviewUrl:
      "https://www.facebook.com/lynne.breznayjerome/posts/pfbid02yXxumWUbWm1GzzfhakLCBBg6Y8ZNvbusxjozWZHyUKew1oQLr3TQFDNADBdd5Sh4l",
  },
  {
    reviewable: facebook,
    quote: `Mike is extremely professional and his work is completed on time and at a great price!`,
    reviewerName: "Michelle W.",
    rooms: [],
    internalScore: 2,
    directReviewUrl:
      "https://www.facebook.com/michelle.laymanwitzky/posts/pfbid02RpDEUbrL3C51DEjstkRVbY1jCMptAAmEahTmwJCjEvD3rPuoyW7zohwCwReSvgL4l",
  },
  {
    reviewable: angi,
    quote: `Mike did all the work himself, which meant no crews coming and going, no bickering, and 1st Class Professionalism throughout!  He gave me a written estimate, and stuck to it to the  penny.
Mike made several suggestions, for efficiency, which were so helpful.  He did little "extras" to personalize it all just for me.
I recommend Mike to anybody looking to do a remodel.  His  craftsmanship, timeliness, and thoroughness are simply magnificent!
I couldn't be happier with the finished project - I have never seen a kitchen as pretty as mine!`,
    reviewerName: "Kay B.",
    rooms: ["kitchen"],
    internalScore: 7,
    //NOTE: She has a facebook url too but going with Angi for better variety
    // directReviewUrl: "https://www.facebook.com/carol.boyd.50159/posts/pfbid0mctGHV424bX7SGYhB7girTrrT1RzwZNU5Dymhi2HyDHAcuLD3RDoQ45g5JeUx2VBl",
  },
  {
    reviewable: google,
    quote: `We are so happy with how our kitchen remodel turned out. It was a gut renovation with some wall, electrical and plumbing updates and it all turned out amazing and on time. Mike and Ralph are easy to work with and true pros. Nice tile work and painting skills too!`,
    reviewerName: "Reynaldo O.",
    rooms: ["kitchen"],
    internalScore: 8,
    directReviewUrl: "https://maps.app.goo.gl/D4ozZ5GMwgue9iyT6",
  },
  {
    reviewable: google,
    quote: `Sullivan Remodels did a fantastic job on my kitchen renovation. Mike and Ralph worked very well together, each one complimenting the other. They were very timely and professional. They offered helpful suggestions and they were very agreeable to changes we requested. They removed our soffits , did electrical work and  much more and all their finish work was top notch we highly recommend them.`,
    reviewerName: "Amelia H.",
    rooms: ["kitchen"],
    internalScore: 8,
    directReviewUrl: "https://maps.app.goo.gl/AGaZ5ceQ9jPWotcXA",
  },
  {
    reviewable: google,
    quote: `Mike and Ralph came in and remodeled my kitchen. New flooring, cabinets, countertops, and paint. With some updated wiring and outlets. New sink with garbage deposal installed.

Everything looks great, and functions great. Mike and Ralph helped me with some ideas I was struggling with and offered me cost effective solutions that also looked good. I am still getting used to the new kitchen because everything is different, and nicer. The paint and trim looks great with the warmer wood colors and the new floor with really compliments everything.

Mike was always quick to answer my questions and he and Ralph worked quickly and efficiently. My cat, Mr. Tiggs, also said they were excellent workers.`,
    reviewerName: "Ryan H.",
    rooms: ["kitchen"],
    internalScore: 8,
    directReviewUrl: "https://maps.app.goo.gl/ux2PgGG1hYM3Fxbx6",
  },
  {
    reviewable: google,
    quote: `We could not be happier with the kitchen remodeling project Mike did for us.  The work was on-time and within budget.  Mike was always very responsive to our questions and requests.  He made certain everything was done to our satisfaction.  We plan to hire him for future work to our house.`,
    reviewerName: "Charles H.",
    rooms: ["kitchen"],
    internalScore: 8,
    directReviewUrl: "https://maps.app.goo.gl/26c1N5TfAgZ7bGnp8",
  },
  {
    reviewable: google,
    quote: `Mike & Ralph did a great job with my kitchen remodel. They were timely, professional, and very communicative. I wouldn't have changed a thing and highly recommend to anyone in the area looking to remodel!`,
    reviewerName: "Colleen A.",
    rooms: ["kitchen"],
    internalScore: 8,
    directReviewUrl: "https://maps.app.goo.gl/Zpb24RcmKnNYXwSC6",
  },
  {
    reviewable: google,
    quote: `Sullivan Remodels did a great job on a kitchen renovation last summer.  The project had many unique challenges however Mike and team were able to address everything on time with great communication.  Sullivan Remodel's attention to detail and great value put them at the top of the list for my next big project.`,
    reviewerName: "Michael G.",
    rooms: ["kitchen"],
    internalScore: 8,
    directReviewUrl: "https://maps.app.goo.gl/9vpR5P2N2pghr4ax6",
  },
  {
    reviewable: google,
    quote: `We recently had our kitchen remodeled by Mike with Sullivan Remodels, and we are pleased with our choice. He was respectful, set accurate expectations for completion, pricing is fair, and he pays attention to detail. We couldn’t be happier with the end result.`,
    reviewerName: "Drew J.",
    rooms: ["kitchen"],
    internalScore: 8,
    directReviewUrl: "https://maps.app.goo.gl/p8h6tDCzc2H6EeTY9",
  },
  {
    reviewable: google,
    quote: `Mike created a 4th bedroom in our Shaker Heights home out of an unfinished attic space, adding heat, electric, walls, carpeting, and got it all done on time and under budget.

He also installed new kitchen countertops, added new outdoor drainage, landscaped, and painted throughout.

Great service and exceptional value!`,
    reviewerName: "Esa-Matti T.",
    rooms: ["kitchen", "attic"],
    internalScore: 8,
    directReviewUrl: "https://maps.app.goo.gl/8CtK8h1kmrpjkgjq7",
  },
  {
    reviewable: google,
    quote: `Mike does an amazing job when it comes to home remodeling or many other projects around the house. I highly recommend him for any hone improvement need.`,
    reviewerName: "Steven F.",
    rooms: [],
    internalScore: 8,
    directReviewUrl: "https://maps.app.goo.gl/TB1nyH1mBK88Pk4z5",
  },
  {
    reviewable: google,
    quote: `Absolutely outstanding experience.  Mike's attention to detail is second to none. Very reasonably priced and the quality of the work he did and the coordination of the entire project was first rate.  I highly recommend this company`,
    reviewerName: "Jack G.",
    rooms: [],
    internalScore: 8,
    directReviewUrl: "https://maps.app.goo.gl/8XpXw3CPTQn2XKYQA",
  },
  {
    reviewable: angi,
    quote: `We could not be happier with the results of our kitchen remodeling project. Cabinets and countertops were replaced and look great. The project was on time and within budget, and Mike was very responsive to all our questions and requests. We plan to hire Mike for future projects and highly recommend his services.`,
    reviewerName: "Angi Reviewer",
    rooms: ["kitchen"],
    internalScore: 8,
    // directReviewUrl: "https://maps.app.goo.gl/8XpXw3CPTQn2XKYQA",
  },
];

import { Reviewable } from "./Reviewable";

export const facebook: Reviewable = {
  profileUrl: "https://www.facebook.com/Sullivan-Remodels-111949937049596",
  reviewReadUrl: `https://www.facebook.com/profile.php?id=100057629731931&sk=reviews`,
  reviewCreateUrl: `https://www.facebook.com/profile.php?id=100057629731931&sk=reviews`, //TODO: How to create new fb review?
  iconUrl: "/Images/facebook.png",
  title: "Facebook",
};

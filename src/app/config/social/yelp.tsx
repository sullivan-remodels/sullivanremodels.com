import { Reviewable } from "./Reviewable";

const yelpUrl = "https://www.yelp.com/biz/sullivan-remodels-lyndhurst";
export const yelp: Reviewable = {
  profileUrl: yelpUrl,
  reviewReadUrl: yelpUrl,
  reviewCreateUrl:
    "https://www.yelp.com/writeareview/biz/0ceXLWB7tGipI9HiiRaqvQ?return_url=%2Fbiz%2F0ceXLWB7tGipI9HiiRaqvQ&review_origin=biz-details-war-button",
  iconUrl: "/Images/yelp.png",
  title: "Yelp",
};

import { Reviewable } from "./Reviewable";

export const angi: Reviewable = {
  profileUrl: "https://member.angi.com/member/store/17555651",
  reviewReadUrl:
    "https://member.angi.com/member/store/17555651/reviews?categoryId=291",
  reviewCreateUrl: "https://www.angi.com/write-review/17555651",
  iconUrl: "/Images/angi.png",
  title: "Angi",
};

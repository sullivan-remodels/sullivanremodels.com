import { Reviewable } from "./Reviewable";

export const googleTagManagerId = "GTM-W4LG2RZ";
export const googleProfileUrl = "https://g.page/SullivanRemodels";
export const google: Reviewable = {
  profileUrl: googleProfileUrl,
  reviewReadUrl: googleProfileUrl,
  reviewCreateUrl: `${googleProfileUrl}/review?rc`,
  iconUrl: "/Images/google.png",
  title: "Google",
};

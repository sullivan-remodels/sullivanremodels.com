import { angi } from "./angi";
import { facebook } from "./facebook";
import { google } from "./google";
import { Reviewable } from "./Reviewable";
import { yelp } from "./yelp";

export const socials: Reviewable[] = [facebook, google, yelp, angi];

export type Reviewable = {
    profileUrl: string;
    reviewReadUrl: string;
    reviewCreateUrl: string;
    iconUrl: string;
    title: string;
};

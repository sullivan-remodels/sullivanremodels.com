import { Room } from "../../photos/Room";
import { Reviewable } from "./Reviewable";

export type ReviewQuote = {
    reviewable: Reviewable;
    reviewerName: string;
    quote: string;
    directReviewUrl?: string;
    rooms: Room[];
    internalScore: number;
};

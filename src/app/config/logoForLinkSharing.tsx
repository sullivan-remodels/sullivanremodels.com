import { canonicalSiteUrl } from "./domain";

export const logoForLinkSharing = `${canonicalSiteUrl}/Images/logoForFacebookShare.png`;

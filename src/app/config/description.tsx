import { cities } from "./cities";

export const description = `Remodeling (Kitchens, Baths, etc) in the Greater Cleveland, Ohio area; including ${cities.join(
  ", "
)}, and nearby cities and towns`;

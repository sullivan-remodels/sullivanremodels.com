import Script from "next/script";

export const JobberContactForm = () => (
  <>
    <div id="905a6800-7f98-485f-8103-29293538dc5a"></div>
    <link
      rel="stylesheet"
      href="https://d3ey4dbjkt2f6s.cloudfront.net/assets/external/work_request_embed.css"
      media="screen"
    />
    <Script
      src="https://d3ey4dbjkt2f6s.cloudfront.net/assets/static_link/work_request_embed_snippet.js"
      {...{
        clienthub_id: "905a6800-7f98-485f-8103-29293538dc5a",
        form_url:
          "https://clienthub.getjobber.com/client_hubs/905a6800-7f98-485f-8103-29293538dc5a/public/work_request/embedded_work_request_form",
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      } as any }
    />
  </>
);

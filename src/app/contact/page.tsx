import { Metadata } from "next";
import Link from "next/link";
import { JobberContactButton } from "../components/contact/JobberContactButton";
import { JobberLink } from "../components/contact/JobberLink";
import { MikeCallLink, MikeTextLink } from "../components/contact/MikeCallLink";
import { MikeEmailLink } from "../components/contact/MikeEmailText";
import { HeroBase } from "../components/layout/HeroBase";
import { MainContentArea } from "../components/layout/MainContentArea";
import { metadataForPage } from "../components/metadata/metadataForPage";
import { ReviewsTeaser } from "../components/reviews/ReviewsTeaser";
import { defaultQuotes } from "../components/reviews/defaultQuotes";
import { jobberClientLogingUrl } from "../config/jobber";
import { mikeEmailAddress } from "../config/mikeEmailAddress";
import { mikePhoneNumber } from "../config/mikePhoneNumber";
import { facebook } from "../config/social/facebook";
import { path } from "./path";
import { JSONLD } from "../components/metadata/jsonld/JSONLD";
import { providerContactUsJSONLD } from "../components/metadata/jsonld/providerContactUsJSONLD";

const title = "Contact Sullivan Remodels";
const description = "Request a quote, email us, call us, text us, or find us on social media!";
const primaryImage = "/Images/contactHero.png";
export const metadata: Metadata = metadataForPage(path, title, description, primaryImage);

export default function ContactPage() {
  return (
    <>
      <JSONLD jsonLD={providerContactUsJSONLD} />
      <HeroBase {...{ primaryImage }}>
        <h1 style={{ marginBottom: "0.5em" }}>{title}</h1>
        <JobberContactButton />
        <p style={{ marginTop: "2em" }}>
          <MikeCallLink>
            <span style={{ color: "white" }}>{mikePhoneNumber}</span>
          </MikeCallLink>
        </p>
        <p>
          <MikeEmailLink>
            <span style={{ color: "white" }}>{mikeEmailAddress}</span>
          </MikeEmailLink>
        </p>
      </HeroBase>
      <MainContentArea>
        <h3>
          Sullivan Remodels knows that timely communication is important to our
          customers!
        </h3>
        <p>
          <strong>New / Potential Customers</strong>, if you need a quote, it is
          best to fill our brief <JobberLink>quote request form</JobberLink>.
        </p>
        <p>
          <strong>Returning Customers</strong> should visit{" "}
          <a href={jobberClientLogingUrl}>customer portal login</a> to view
          quote requests, bookings, bills and more!
        </p>
        <p>
          You may also <MikeTextLink>text</MikeTextLink>,{" "}
          <MikeCallLink>phone</MikeCallLink> or{" "}
          <MikeEmailLink>email</MikeEmailLink> us any time.
        </p>
        <Link href={facebook.profileUrl} target="_blank">
          <img src="/Images/find-us-on-facebook.jpg" width="100" height="33" />
        </Link>
        <p>
          <strong>Thanks</strong>,
        </p>
        <p>
          <strong>Mike Sullivan</strong>
          <br />
          Owner
          <br />
          Sullivan Remodels
        </p>
      </MainContentArea>
      <ReviewsTeaser reviewQuotes={defaultQuotes} />
    </>
  );
}

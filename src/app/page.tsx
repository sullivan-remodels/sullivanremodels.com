import { Page } from "./components/layout/Page";
import { providerJSONLD } from "./components/metadata/jsonld/providerJSONLD";
import { metadataForPage } from "./components/metadata/metadataForPage";
import { RemodelingTypes } from "./components/nav/RemodelingTypes";
import { defaultQuotes } from "./components/reviews/defaultQuotes";
import { cities } from "./config/cities";
import { description } from "./config/description";

const path = "/";
const title = "Sullivan Remodels";
const metaTitle = `${title} - Welcome!`;
const primaryImage = "/Images/homeHero.png";
const subtitle = "Complete Home Remodeling";

export const metadata = metadataForPage(
  path,
  metaTitle,
  description,
  primaryImage
);

export default function Home() {
  return (
    <Page
      {...{
        jsonLD: providerJSONLD,
        primaryImage,
        reviewQuotes: defaultQuotes,
        subtitle,
        title,
      }}
    >
      <header className="pageTitle section-header">
        Trusted Professionals Since 2008
      </header>
      <p>
        Kitchen and home remodeling in {cities.join(", ")} and other nearby
        areas in Cuyahoga County!
      </p>
      <RemodelingTypes />
    </Page>
  );
}

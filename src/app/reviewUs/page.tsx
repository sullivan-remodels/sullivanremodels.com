import Link from "next/link";
import { metadataForServicePage } from "../components/metadata/metadataForServicePage";
import { canonicalSiteUrl } from "../config/domain";
import { google } from "../config/social/google";
import { ReviewUsIcons } from "../components/reviews/ReviewUsIcons";

const url = `${canonicalSiteUrl}/reviewUs`;
const title = "Review Us @ Sullivan Remodels";
const description =
  "Write a review for Sullivan Remodels on a trusted review platform like Google, Yelp, Angi or Facebook!";

const pageImage = "/Images/reviewUsHero.png";

export const metadata = metadataForServicePage(url, title, description, pageImage);
export default function ReviewUs() {
  return (
    <>
      <Link
        href={google.reviewCreateUrl}
        target="_blank"
        style={{ textDecoration: "none" }}
      >
        <div className="hero" style={{ backgroundImage: `url(${pageImage})` }}>
          <div className="hero-text">
            <h1>Review Us</h1>
            <h2>Your Feedback Helps Us Grow</h2>
          </div>
        </div>
      </Link>
      <div id="main" className="darkTextLightBG last-section">
        <p className="section-header">Tell Us What You Think</p>
        <p className="section-subtitle">We Value Your Input</p>
        <h3>Click any of the links below to navigate to our review pages!</h3>
        <ReviewUsIcons />
      </div>
    </>
  );
}

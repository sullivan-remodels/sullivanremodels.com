import { PhotoBase } from "../photos/Photo";
import { Room } from "../photos/Room";

export const sortedPhotos = (room: Room) => <T extends PhotoBase>(photos: T[]):T[] => photos
  .sort((a, b) => b.score - a.score)
  .filter((p) => p.room == room)
  .filter((p) => !p.isSizeDifference);

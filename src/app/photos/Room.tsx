
export type Room = "kitchen" |
  "bathroom" |
  "basement" |
  "attic" |
  "homeOffice" |
  "other" |
  "livingRoom" |
  "diningRoom" |
  "pantry";


export type Colors = "white" | "black" | "gray" | "blue" | "beige"
export type Features = "steps" | "floor" | "fireplace"
export type Tag = Colors | Features

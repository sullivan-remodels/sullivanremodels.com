import { Room } from "./Room";
import { Tag } from "./Tag";
import { Score } from "./Score";

export type BeforeAndAfterPhoto = PhotoBase & {
  beforeImage: string;
  afterImage: string;
};

export type AfterPhoto = PhotoBase & { href: string }

export type PhotoBase = {
  room: Room;
  tags: Tag[];
  score: Score;
  isSizeDifference?: boolean;
}
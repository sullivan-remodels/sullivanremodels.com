import { path } from "./path";
import { ServiceData } from "../../components/metadata/ServiceData";

export const atticServiceData: ServiceData = {
    shortName: "Attics",
    serviceName: "Attic Remodeling",
    description: "Old dusty attics can become full masters suites, gymnasium, playrooms and more with the experts from Sullivan Remodels!",
    path,
    primaryImage: "/Images/attics/atticAfter.jpg",
};

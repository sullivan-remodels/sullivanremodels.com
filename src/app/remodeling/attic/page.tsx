import { ServicePage } from "../../components/layout/ServicePage";
import { metadataForServicePage } from "../../components/metadata/metadataForServicePage";
import { AtticPhotos } from "./AtticPhotos";
import { atticServiceData } from "./atticServiceData";
import { path } from "./path";
import { photos } from "./photos";
import { room } from "./room";

const subtitle = "Transform into Bath, Bedroom + more!";
const serviceOfferingNames=["Bedroom Additions", "Attic Remodeling"];
const { description, serviceName, primaryImage } = atticServiceData;

export const metadata = metadataForServicePage(
  path,
  serviceName,
  description,
  primaryImage
);

export default function AtticPage() {
  return (
    <ServicePage
      {...{
        description,
        serviceName,
        serviceOfferingNames,
        primaryImage,
        subtitle,
        room,
      }}
      photoHrefs={photos.map((p) => p.afterImage)}
    >
      <AtticPhotos />
    </ServicePage>
  );
}

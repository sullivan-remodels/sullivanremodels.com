import { BeforeAndAfterPhoto } from "../../photos/Photo";
import { room } from "./room";

export const photos: BeforeAndAfterPhoto[] = [
  {
    beforeImage: "/Images/attics/atticBefore.jpg",
    afterImage: "/Images/attics/atticAfter.jpg",
    room: room,
    tags: ["white"],
    score: 8,
    isSizeDifference: false,
  },
];

import { BeforeAndAfterTable } from "../../components/photos/BeforeAndAfterTable";
import { photos } from "./photos";
import { room } from "./room";

export const AtticPhotos = () => (
  <BeforeAndAfterTable
    description="Have a look at these beautiful attic remodels!"
    {...{ room, photos }}
  >
    <div className="photo-grid-item">
      <div className="video-container">
        <iframe
          width="560"
          height="315"
          src="https://www.youtube.com/embed/aaIDKxIwh64?si=jwT6CGqIja0ulJgT"
          title="YouTube video player"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
        ></iframe>
      </div>
      <p>Before</p>
    </div>
    <div className="photo-grid-item">
      <div className="video-container">
        <iframe
          width="560"
          height="315"
          src="https://www.youtube.com/embed/6mlyw3Vxc74?si=HXsPTGzUA0Mhgk6E"
          title="YouTube video player"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
        ></iframe>
      </div>
      <p>After</p>
    </div>
  </BeforeAndAfterTable>
);

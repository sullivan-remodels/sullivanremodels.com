import { path } from "./path";
import { ServiceData } from "../../components/metadata/ServiceData";

export const basementServiceData: ServiceData = {
    shortName: "Basements",
    serviceName: "Basement Remodeling",
    description: "Everything to convert your basement into new, livable space: Walls, Ceilings, Flooring, Electric, Lighting, HVAC, etc.",
    path,
    primaryImage: "/Images/basement/basement1.jpg",
};

import { ServicePage } from "../../components/layout/ServicePage";
import { metadataForServicePage } from "../../components/metadata/metadataForServicePage";
import { afterPhotos } from "./afterPhotos";
import { BasementPhotos } from "./BasementPhotos";
import { basementServiceData } from "./basementServiceData";
import { path } from "./path";
import { room } from "./room";

const subtitle = "Walls, Ceilings, Flooring, Electric, Lighting and HVAC!"
const serviceOfferingNames=[
  "Basement Drywall Installation",
  "Basement Carpet Installation",
  "Basement Flooring Installation",
  "Basement Tiling Installation",
  "Basement Lighting Installation",
  "Bar or Minibar Installation",
]

const { description, serviceName, primaryImage } = basementServiceData;

export const metadata = metadataForServicePage(path, serviceName, description, primaryImage);

export default function BasementPage() {
  return (
    <ServicePage
      {...{
        description,
        serviceName,
        serviceOfferingNames,
        primaryImage,
        subtitle,
        room,
      }}
      photoHrefs={afterPhotos.map((p) => p.href)}
    >
      <BasementPhotos />
    </ServicePage>
  );
}

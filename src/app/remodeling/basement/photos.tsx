import { BeforeAndAfterPhoto } from "../../photos/Photo";
import { room } from "./room";

export const photos: BeforeAndAfterPhoto[] = [
  {
    beforeImage: "/Images/basement/basementRemodel2Before.jpg",
    afterImage: "/Images/basement/basementRemodel2After.png",
    isSizeDifference: false,
    room,
    tags: ["floor"],
    score: 7,
  },
  {
    beforeImage: "/Images/basement/basementFlooringBefore.jpg",
    afterImage: "/Images/basement/basementFlooringAfter.jpg",
    room,
    tags: ["white", "steps"],
    score: 7,
  },
  {
    beforeImage: "/Images/basement/stairsBefore.jpg",
    afterImage: "/Images/basement/stairsAfter.jpg",
    room,
    tags: ["white", "steps"],
    score: 4,
  },
  {
    beforeImage: "/Images/basement/stairs2Before.jpg",
    afterImage: "/Images/basement/stairs2After.jpg",
    room,
    tags: ["white", "steps"],
    score: 4,
  },
  {
    beforeImage: "/Images/basement/basementPaintAndFloorBefore.jpg",
    afterImage: "/Images/basement/basementPaintAndFloorAfter.jpg",
    room,
    tags: ["white"],
    score: 5,
  },
  {
    beforeImage: "/Images/basement/basementRemodelBefore.jpg",
    afterImage: "/Images/basement/basementRemodelAfter.jpg",
    room,
    tags: ["white"],
    score: 7,
  },
  {
    beforeImage: "/Images/basement/minibarBefore.jpg",
    afterImage: "/Images/basement/minibarAfter.jpg",
    room,
    tags: ["white", "beige"],
    score: 9,
  },
];

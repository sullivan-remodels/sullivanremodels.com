import { AfterPhoto } from "../../photos/Photo";
import { room } from "./room";

export const afterPhotos: AfterPhoto[] = [
  {
    href: "/Images/basement/basement1.jpg",
    room,
    score: 10,
    tags: ["gray"],
  },
  {
    href: "/Images/basement/basement1b.jpg",
    room,
    score: 10,
    tags: ["gray", "fireplace"],
  },
];

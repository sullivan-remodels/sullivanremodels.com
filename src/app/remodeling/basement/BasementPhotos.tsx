import { AfterPhotosSection } from "../../components/photos/AfterPhotosSection";
import { BeforeAndAfterTable } from "../../components/photos/BeforeAndAfterTable";
import { afterPhotos } from "./afterPhotos";
import { photos } from "./photos";
import { room } from "./room";

export const BasementPhotos = () => (
  <>
    <BeforeAndAfterTable description="Have a look at these beautiful basement remodels!" {...{room, photos}} />
    <AfterPhotosSection {...{ room, afterPhotos }} />
  </>
);

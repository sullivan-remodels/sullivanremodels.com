import { path } from "./path";
import { ServiceData } from "../../components/metadata/ServiceData";

export const kitchenServiceData: ServiceData = {
    shortName: "Kitchens",
    serviceName: "Kitchen Remodeling",
    description: "New or Updated Cabinets, Counters, Sinks, Lighting, Appliances, Floors, Painting, Windows and Hardware, including Design Consultation, for your kitchen remodel!",
    path,
    primaryImage: "/Images/kitchen/kitchen3.jpg",
};

import { ServicePage } from "../../components/layout/ServicePage";
import { metadataForServicePage } from "../../components/metadata/metadataForServicePage";
import { afterPhotos } from "./afterPhotos";
import { KitchenPhotos } from "./KitchenPhotos";
import { path } from "./path";
import { room } from "./room";
import { kitchenServiceData } from "./kitchenServiceData";

const subtitle =
"Cabinets, Counters, Sinks, Lighting, Appliances, Floors + Structure!";

const serviceOfferingNames = [
  "Custom Cabinet Installation",
  "Countertop Installation",
  "Sink Installation",
  "Appliance Installation",
  "Wall + Structural Changes",
  "Lighting Installation",
  "Flooring Installation",
  "Kitchen Design Services",
];

const { description, serviceName, primaryImage } = kitchenServiceData;

export default function KitchenPage() {
  return (
    <ServicePage
      {...{
        description,
        serviceName,
        serviceOfferingNames,
        primaryImage,
        subtitle,
        room,
      }}
      photoHrefs={afterPhotos.map((p) => p.href)}
    >
      <KitchenPhotos />
    </ServicePage>
  );
}

export const metadata = metadataForServicePage(
  path,
  serviceName,
  description,
  primaryImage
);

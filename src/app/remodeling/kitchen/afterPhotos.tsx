import { AfterPhoto } from "../../photos/Photo";
import { room } from "./room";

export const afterPhotos: AfterPhoto[] = [
  {
    href: "/Images/kitchen/breakfastNook.jpg",
    room,
    score: 6,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/Carol after2.jpg",
    room,
    score: 4.5,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/Carol after3.jpg",
    room,
    score: 4,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/Blair after4.jpg",
    room,
    score: 3,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/Bursky3 after.jpg",
    room,
    score: 3,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/dianaKitchenAfter2.jpg",
    room,
    score: 3,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/Heller1 after.jpg",
    room,
    score: 0,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/Heller2 after.jpg",
    room,
    score: 5,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/jack after1.jpg",
    room,
    score: 2,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/jack after2.jpg",
    room,
    score: 2,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/kitchen1.jpg",
    room,
    score: 6,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/kitchen1b.jpg",
    room,
    score: 6,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/kitchen2.jpg",
    room,
    score: 6,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/kitchen3.jpg",
    room,
    score: 10,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/kitchen5.jpg",
    room,
    score: 10,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/kitchen6.jpg",
    room,
    score: 10,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/_kitchen9.jpg",
    room,
    score: 10,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/kitchen11stoveWithShinyTiles.jpg",
    room,
    score: 9,
    tags: [],
  },
  {
    href: "/Images/kitchen/kitchen7Trashbags.jpg",
    room,
    score: 0,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/kitchen4bluecabinets.jpg",
    room,
    score: 1,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/cydKitchenAfter.jpg",
    room,
    score: 1,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/cydKitchenAfter2.jpg",
    room,
    score: 1,
    tags: ["beige"],
  },
  {
    href: "/Images/kitchen/_kitchen8shinyteal1f.jpg",
    room,
    score: 6,
    tags: ["beige"],
  },
];

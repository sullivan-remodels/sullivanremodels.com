import { AfterPhotosSection } from "../../components/photos/AfterPhotosSection";
import { BeforeAndAfterTable } from "../../components/photos/BeforeAndAfterTable";
import { afterPhotos } from "./afterPhotos";
import { beforeAndAfterPhotos } from "./beforeAndAfterPhotos";
import { room } from "./room";
import { withDecentScore } from "../../components/photos/withDecentScore";

export const KitchenPhotos = () => (
  <>
    <BeforeAndAfterTable
      description="Have a look at these beautiful kitchen remodels!"
      {...{
        photos: beforeAndAfterPhotos.filter(withDecentScore),
        room,
      }}
    />
    <AfterPhotosSection
      afterPhotos={afterPhotos.filter(withDecentScore)}
      room="kitchen"
    />
  </>
);


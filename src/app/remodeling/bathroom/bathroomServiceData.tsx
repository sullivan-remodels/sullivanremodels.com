import { path } from "./path";
import { ServiceData } from "../../components/metadata/ServiceData";

export const bathroomServiceData: ServiceData = {
    shortName: "Bathrooms",
    serviceName: "Bathroom Remodeling",
    description: "Bathroom flooring, tile work, showers, sinks, vanities and structural changes, including Design Consultation, for your bathroom remodel!",
    path,
    primaryImage: "/Images/bath/bathroom-shower-after.jpg",
};

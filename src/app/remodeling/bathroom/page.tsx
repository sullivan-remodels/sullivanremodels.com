import { ServicePage } from "../../components/layout/ServicePage";
import { metadataForServicePage } from "../../components/metadata/metadataForServicePage";
import { afterPhotos } from "./afterPhotos";
import { BathroomPhotos } from "./BathroomPhotos";
import { bathroomServiceData } from "./bathroomServiceData";
import { path } from "./path";
import { room } from "./room";

const subtitle = "Tiling, Floors, Showers, Sinks, Vanities + Structure!";

const serviceOfferingNames=[
  "Bathroom Floor Installation",
  "Shower Installation",
  "Tub Installation",
  "Vanity Installation",
  "Tile Installation",
  "Bathroom Lighting Installation",
  "Bidet Installation",
  "Toilet Installation",
  "Bathroom Redesign",
];

const { description, serviceName, primaryImage } = bathroomServiceData;

export const metadata = metadataForServicePage(
  path,
  serviceName,
  description,
  primaryImage
);

export default function BathroomPage() {
  return (
    <ServicePage
      {...{
        description,
        serviceName,
        serviceOfferingNames,
        primaryImage,
        subtitle,
        room,
      }}
      photoHrefs={afterPhotos.map((p) => p.href)}
    >
      <BathroomPhotos />
    </ServicePage>
  );
}

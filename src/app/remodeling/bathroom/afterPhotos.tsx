import { AfterPhoto } from "../../photos/Photo";
import { room } from "./room";

export const afterPhotos: AfterPhoto[] = [
  {
    href: "/Images/bath/bathroom-2-shower-after.jpg",
    room,
    score: 6,
    tags: [],
  },
  {
    href: "/Images/bath/bathroom-3-after.jpg",
    room,
    score: 7,
    tags: [],
  },
  {
    href: "/Images/bath/bathroom-3-shower-after.jpg",
    room,
    score: 3,
    tags: [],
  },
  {
    href: "/Images/bath/bathroom-4.png",
    room,
    score: 8,
    tags: [],
  },
  {
    href: "/Images/bath/bathroom-5.jpg",
    room,
    score: 9,
    tags: [],
  },
  {
    href: "/Images/bath/bathroom-6.jpg",
    room,
    score: 6,
    tags: [],
  },
  {
    href: "/Images/bath/bathroom-7.jpg",
    room,
    score: 7,
    tags: [],
  },
  {
    href: "/Images/bath/bathroom-shower-after.jpg",
    room,
    score: 9,
    tags: [],
  },
  {
    href: "/Images/bath/bathroom-sink-after.jpg",
    room,
    score: 2,
    tags: [],
  },
  {
    href: "/Images/bath/shower1.jpg",
    room,
    score: 2,
    tags: [],
  },
];

import { AfterPhotosSection } from "../../components/photos/AfterPhotosSection";
import { BeforeAndAfterTable } from "../../components/photos/BeforeAndAfterTable";
import { BeforeAndAfterPhoto } from "../../photos/Photo";
import { afterPhotos } from "./afterPhotos";
import { room } from "./room";

const photos: BeforeAndAfterPhoto[] = [
  {
    beforeImage: "/Images/bath/bathroomWallRemovalBefore.jpg",
    afterImage: "/Images/bath/bathroomWallRemovalAfter.jpg",
    room,
    tags: ["white"],
    score: 6,
    isSizeDifference: false,
  },
  {
    beforeImage: "/Images/bath/bathroomRemodelBefore.jpg",
    afterImage: "/Images/bath/bathroomRemodelAfter.jpg",
    room,
    tags: ["white"],
    score: 10,
    isSizeDifference: false,
  },
];

export const BathroomPhotos = () => (
  <>
    <BeforeAndAfterTable
      description="Have a look at these beautiful bathroom remodels!"
      {...{ room, photos }}
    />
    <AfterPhotosSection room="bathroom" {...{ afterPhotos }} />
  </>
);

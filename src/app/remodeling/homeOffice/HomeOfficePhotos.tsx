import { AfterPhotosSection } from "../../components/photos/AfterPhotosSection";
import { BeforeAndAfterTable } from "../../components/photos/BeforeAndAfterTable";
import { afterPhotos } from "./afterPhotos";
import { photos } from "./photos";
import { room } from "./room";

export const HomeOfficePhotos = () => (
  <>
    <BeforeAndAfterTable description="Have a look at these mockup examples of beautiful home office remodels!" {...{photos, room}} />
    <AfterPhotosSection room="homeOffice" {...{ afterPhotos }} />
  </>
);

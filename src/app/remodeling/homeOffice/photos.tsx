import { BeforeAndAfterPhoto } from "../../photos/Photo";
import { room } from "./room";

export const photos: BeforeAndAfterPhoto[] = [
  {
    beforeImage: "/Images/homeOffice/before.webp",
    afterImage: "/Images/homeOffice/after.webp",
    room,
    tags: ["white"],
    score: 8,
    isSizeDifference: false,
  },
];

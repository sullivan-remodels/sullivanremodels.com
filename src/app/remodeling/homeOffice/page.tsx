import { ServicePage } from "../../components/layout/ServicePage";
import { metadataForServicePage } from "../../components/metadata/metadataForServicePage";
import { afterPhotos } from "./afterPhotos";
import { HomeOfficePhotos } from "./HomeOfficePhotos";
import { homeOfficeServiceData } from "./homeOfficeServiceData";
import { path } from "./path";
import { room } from "./room";

const subtitle =
  "Custom Cabinets, Desk Built-Ins, Lighting, Window Framing, Flooring + more!";

const serviceOfferingNames = [
  "Custom Office Cabinets + Desks",
  "Office Lighting Installation",
  "Painting",
  "Wallpaper Removal",
];

const { description, serviceName, primaryImage } = homeOfficeServiceData;

export const metadata = metadataForServicePage(
  path,
  serviceName,
  description,
  primaryImage
);

export default function HomeOfficePage() {
  return (
    <ServicePage
      {...{
        description,
        serviceName,
        serviceOfferingNames,
        primaryImage,
        subtitle,
        room,
      }}
      photoHrefs={afterPhotos.map((p) => p.href)}
    >
      <HomeOfficePhotos />
    </ServicePage>
  );
}

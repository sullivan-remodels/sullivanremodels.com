import { AfterPhoto } from "../../photos/Photo";
import { room } from "./room";

export const afterPhotos: AfterPhoto[] = [
  {
    href: "/Images/homeOffice/customCabinets.jpg",
    room,
    score: 8,
    tags: [],
  },
];

import { path } from "./path";
import { ServiceData } from "../../components/metadata/ServiceData";

export const homeOfficeServiceData: ServiceData = {
    shortName: "Home Offices",
    serviceName: "Home Office Remodeling",
    description: "Home Office featuring Custom Cabinetry, Desk Built-Ins, Luxury Wall Molding, New Lighting, Window Framing, Flooring and more!",
    path,
    primaryImage: "/Images/homeOffice/customCabinets.jpg",
};

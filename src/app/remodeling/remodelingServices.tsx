import { atticServiceData } from "./attic/atticServiceData";
import { basementServiceData } from "./basement/basementServiceData";
import { bathroomServiceData } from "./bathroom/bathroomServiceData";
import { homeOfficeServiceData } from "./homeOffice/homeOfficeServiceData";
import { kitchenServiceData } from "./kitchen/kitchenServiceData";
import { ServiceData } from "../components/metadata/ServiceData";

export const remodelingServices: ServiceData[] = [
  kitchenServiceData,
  bathroomServiceData,
  basementServiceData,
  atticServiceData,
  homeOfficeServiceData,
];

import { ServiceCatalogPage } from "../components/layout/ServiceCatalogPage";
import { metadataForPage } from "../components/metadata/metadataForPage";
import { RemodelingTypes } from "../components/nav/RemodelingTypes";
import { defaultQuotes } from "../components/reviews/defaultQuotes";
import { metaTitle } from "./metaTitle";
import { path } from "./path";

const heroTitle = "Remodeling";
const description =
  "Remodel your Kitchen, Bathroom, Attic, Basement, or other space with the experts at Sullivan Remodels!";
const subtitle = "Kitchens, Baths, Basements, Attics + More";

const primaryImage = "/Images/remodelHero.png";

export const metadata = metadataForPage(path, metaTitle, description, primaryImage);

export default function RemodelingPage() {
  return (
    <ServiceCatalogPage
      {...{
        description,
        heroTitle,
        metaTitle,
        primaryImage,
        subtitle,
        reviewQuotes: defaultQuotes,
      }}
    >
      <p className="section-header">Highly Skilled Craftsmanship</p>
      <p>
        We offer best-in-class remodeling / renovation services at competitive
        prices. Our expertise includes:
      </p>
      <ul className="hList">
        <li>general renovations / remodeling</li>
        <li>indoor + outdoor painting</li>
        <li>deck power washing</li>
        <li>deck refinishing</li>
        <li>demolition</li>
        <li>wall construction</li>
        <li>lighting installation</li>
        <li>tile flooring</li>
        <li>sink installation</li>
        <li>appliance installation</li>
      </ul>
      <p className="section-header" style={{ marginTop: "1em" }}>
        Featured Remodeling Types
      </p>
      <p>View some of our most popular room remodels:</p>
      <RemodelingTypes />
    </ServiceCatalogPage>
  );
}

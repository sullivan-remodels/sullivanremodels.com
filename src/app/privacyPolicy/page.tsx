import Link from "next/link";
import { MikeEmailText } from "../components/contact/MikeEmailText";
import { metadataForServicePage } from "../components/metadata/metadataForServicePage";
import { canonicalSiteUrl } from "../config/domain";

const url = `${canonicalSiteUrl}/privacyPolicy`;
const title = "Privacy Policy @ Sullivan Remodels";
const description =
  "Privacy Policy - Sullivan Remodels";

export const metadata = metadataForServicePage(url, title, description);

export default function PrivacyPolicy(){

	return (
		<>
		    <div id="main" className="darkTextLightBG">
				<p className="section-header">Sullivan Remodels Privacy</p>
				<p className="section-subtitle">Your information is safe with Us</p>
				<p>
					Protecting your private information is our priority. This Statement of Privacy applies to www.sullivanremodels.com, 
					Sullivan Remodels, and Mike’s Painting and Remodeling LLC and governs data collection and usage. For the purposes of 
					this Privacy Policy, unless otherwise noted, all references to Sullivan Remodels include www.sullivanremodels.com 
					and Mike’s Painting and Remodeling LLC. The Sullivan Remodels website is a small business site. By using Sullivan 
					Remodels website, you consent to the data practices described in this statement.
				</p>
				<h3>Collection of your Personal Information</h3>
				<p>
					We do not collect any personal information about you unless you voluntarily provide it to us. However, you may be 
					required to provide certain information to us when you elect to use certain products or services available on this Site. 
					These may include sending us an email message. To wit, we will use your information for, but not limited to, communicating 
					with you in relation to services and/or products you have requested from us. We also may gather additional personal 
					information or non-personal information in the future.
				</p>
				<h3>Sharing Information with Third Parties</h3>
				<p>
					Sullivan Remodels does not sell, rent, or lease its customer lists to third parties.
				</p>
				<p>
					Sullivan Remodels may share data with trusted third parties to perform statistical analysis on website traffic and lead 
					generation. All such third parties are prohibited from using your personal information except to provide these services 
					to Sullivan Remodels, and they are required to maintain confidentiality of your information.
				</p>
				<p>
					Sullivan Remodels may disclose your personal information, without notice, if required to do so by law or in the good faith 
					belief that such action is necessary to: (a) conform to the edicts of the law, or comply with legal processes served on Sullivan 
					Remodels or the site; (b) protect and defend the rights or property of Sullivan Remodels; and/or act under exigent circumstances 
					to protect the personal safety of users of Sullivan Remodels, or the public.
				</p>
				<h3>Automatically Collected Data</h3>
				<p>
					Information about your computer hardware and software may be collected automatically by Sullivan Remodels. This information can 
					include: your IP address, browser type, domain names, access times, and referring website addresses. This information is used for 
					the operation of the service to maintain quality of the service, and to provide general statistics regarding use of the Sullivan 
					Remodels website.
				</p>
				<h3>Children Under Thirteen</h3>
				<p>
					Sullivan Remodels does not knowingly collect personally identifiable information from children under the age of thirteen. If you 
					are under the age of thirteen, you must ask your parent or guardian for permission to use this website.
				</p>
				<h3>Email Communications</h3>
				<p>
					From time to time, Sullivan Remodels may contact you via email for the purpose of providing announcements, promotional offers, alerts, 
					confirmations, surveys, and/or other general communications.
				</p>
				<h3>Changes to this Statement</h3>
				<p>
					Sullivan Remodels reserves the right to change this Privacy Policy from time to time. We will notify you about significant changes in 
					the way we treat personal information by sending a notice to the primary email address specified by previous correspondence, by placing 
					a prominent notice on our site, and/or by updating any privacy information on this page. Your continued use of the Site and/or Services 
					available through the Site after such modifications will constitute your: (a) acknowledgement of the modified Privacy Policy; and (b) 
					agreement to abide and be bound by that Policy.
				</p>
				<h3>Contact Information</h3>
				<p>
					Sullivan Remodels welcomes your questions or comments regarding this Statement of Privacy. If you believe that Sullivan Remodels has not 
					adhered to this Statament, please contact Sullivan Remodels.
				</p>
				<p>Mike Sullivan</p>
				<p><Link href="tel:4406662576">440.666.2576</Link></p>
				<p><MikeEmailText /></p>
				<p>Effective as of February 19, 2020</p>
			</div>
		</>
	)
}
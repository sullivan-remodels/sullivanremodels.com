sullivanremodels.com
==================

# Registrar + DNS @ Godaddy

The site is registered on godaddy, the account number is 50214563, mike knows the password. The DNS is also hosted there. The CNAME records for www point to the "DNS Target" of the Custom Domains on Heroku.

- [paintcleveland.com dns](https://dcc.godaddy.com/manage/paintcleveland.com/dns) (`DEPRECATION WARNING`)
- [sullivanremodels.com dns](https://dcc.godaddy.com/manage/SULLIVANREMODELS.COM/dns)

# Hosting @ Heroku

The site is hosted on [heroku](https://dashboard.heroku.com/apps/sullivanremodels), username is mike@sullivanremodels.com.

SIDENOTE: The app was created via `npx heroku create sullivanremodels`

SIDENOTE: [Custom domains](https://dashboard.heroku.com/apps/sullivanremodels/settings) were manually added after the creation of the app for the www subdomains, and the generated "DNS Target" for each was then copied into the CNAME for godaddy's www record for each respective site:

- www.paintcleveland.com
- www.sullivanremodels.com

# Development + Deployment Environment

The development and deployment environment can be created by cloning this repo and then using VS Code's `Dev Container` to automatically build and open a Docker container based development environment, from which one can run the local website, make changes, and ultimately deploy. Note the web server automatically runs, and outputs the URL to the terminal window, as well as creates a link in the `ports` tab of VS-Code.

## Next.js + React

The site is built with next.js and react. The [next generated docs](./next.md) have more details.

# Deployment

## Automated Upon Master Merge

Our [gitlab](./gitlab-ci.yml) is configured to deploy upon master merge.

To automatically deploy the site, merge your working branch with master and await the successful completion of the CICD job. A green check indicates that the test have passed and the newest version of the master branch has been deployed to production.

SIDENOTE: The credential used by gitlab was created via `npx heroku authorizations:create` and added to the gitlab env vars as var `HEROKU_API_KEY` under settings / cicd / variables.

## Manual Deployment

The site can be manually deployed from within VS Code, assuming this repo is cloned, and opened within the defined [DevContainer](./.devcontainer/devcontainer.json)

### Deploying the Local Codebase on Disk

Login, Build, Test, Push and Deploy by running: `./dev manualBuildTestPushAndDeploy` from within the dev container.

This will build, push, and deploy whatever codebase you currently have on local disk, and tag the pushed image with the commit sha, and an optional `-dirty` suffix if there are uncommitted changes on disk. It will also tag this image as `latest`

### Deploying an Earlier Version without Rebuild

NOTE: Only the `latest` tag is ever deployed (a Heroku limitation).

Therefore, one must either push and tag a new image as `latest`, or otherwise tag an existing image as `latest` in the heroku remote. Google how to do it or ask chatgpt.

NOTE: One may override the version string used for image tags via setting the `CURRENT_VERSION` env var. Note such image will still receive the `latest` tag.

# User Analytics + Marketing

We use several tools for tracking user behavior and marketing, some of which have their own readmes for our specific use of them:

- [Google Marketing - (including Tag Manager, Analytics and Search Console)](./google/readme.md)
- [Meta / Facebook - Pixel](#) - NOTE: Near future
- [LinkedIn](#) - NOTE: Near Future

# Quote Requests, Quotes, Job Management, Billing, Follow Up

This is all managed in jobber.

[Jobber Admin Home](https://secure.getjobber.com/home)

# Other Links

- [credentials](https://docs.google.com/document/d/1EX7TUxQCqM8tSmU4Fk79GWKXwNXpgdTKcRclIUThjYU/edit?folder=1nHwumiJDFjYahoIxQjVy38QtM3Ml6abc)
- [google drive folder](https://drive.google.com/drive/folders/1nHwumiJDFjYahoIxQjVy38QtM3Ml6abc)
- [google my business](https://business.google.com/edit/l/07373135719115609949)

# Social / Contracting / Lead Gen Sites

- [Yelp](https://www.yelp.com/biz/sullivan-remodels-lyndhurst)
- [Facebook](https://www.facebook.com/profile.php?id=100057629731931)
  - NOTE: [Old facebook profile](https://www.facebook.com/Sullivanremodels/) still exists with desirable url, but "Mike's Contracting" name is allegedly unchangeable
- [Angies List](https://member.angi.com/member/store/17555651/reviews?categoryId=291)
- [Google](https://member.angi.com/member/store/17555651/reviews?categoryId=291)

## Still Needs Setup:

- [Instagram]()
- [Youtube]()

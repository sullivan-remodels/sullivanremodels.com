Google Integrations
===================

- [Google Tag Manager](./tag-manager/readme.md) which is a "hub" for sending user tracking data to Google Analytics and potentially other tracking tools (eg: Facebook Pixel)
- [Google Analytics](./analytics/readme.md) is where we do analytics about user behavior
- [google search console](https://search.google.com/search-console?resource_id=sc-domain%3Asullivanremodels.com) - This can be used to understand / improve how google crawls the site
Google Analytics
================

# Overview

[GA Homepage](https://analytics.google.com/analytics/web/#/p366136189) is the starting point for analyzing traffic and user behaviors and journeys. Explore `Reports` on the left to understand what users are doing at various points in the lifecycle.

# Testing Changes to Google Tag Manager

After making changes to GTM that might impact GA, it is important to test before proceeding. 

1. Click Admin in the bottom left nav of GA (gear icon)
2. Scroll down and click `DebugView`
3. You should see GA events from GTM show up in a vertical time axis, and you should inspect each to ensure the variables are set as expected
4. After publishing changes, in a day or two, those events should show up within GA reports, and you can further test or tweak from that point.
    - NOTE: There is a "live view" in GA that can help get you quicker feedback on a limited set of recent analytics data.

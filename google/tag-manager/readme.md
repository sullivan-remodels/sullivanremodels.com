Google Tag Manager
==================

# Purpose

This allows non-coders to add various integrations for tracking / marketing, such as Google Analytics or Meta Pixel, without making code changes.

# Account

[Our GTM Account](https://tagmanager.google.com/#/container/accounts/6000793435) can have one or several "workspaces", where we create / update / delete "triggers" and "tags" as well as configure variables that support doing so.

# Updating / Changing GTM
=========================

NOTE: [gtm-config.json](./gtm-config.json) contains the latest GTM settings in JSON form (in case the web version is lost). _Please keep it up to date when you publish versions!!_

1. Make changes in the `workspace` of your choice - these will not be published automatically so you are free to explore and change without worrying
2. Make sure to click `Preview` in the upper right, and enter the url of the system under test (likely `http://localhost:3000` for local changes, or `https://www.sullivanremodels.com` if you're making changes that don't require local code changes that don't or shouldn't exist in production)
    - NOTE: You need to re-click `Preview` if you make more changes in the workspace, or you won't see those changes
    - `Google Tag Assistant` is the name of the tool that pops up to help you inspect whats happening in GTM
    - The site will also pop up in its own window, in a special "debug mode" connected to your tag assistant.

3. Click around in the site and observe and inspect the results in your `Google Tag Assistant` to confirm they work as expected (inspect the tags fired and the variable values in each event)

4. Test outbound integrations that might be impacted by your changes:

- See [Google Analytics](../analytics/readme.md) readme for help running debug mode for such testing
- Other technologies will have their own way of debugging, add quick readme links here for those as they are added.

5. Click publish and add a meaningful brief title and optional description. Don't use version numbers since google automatically uses integers for versions.

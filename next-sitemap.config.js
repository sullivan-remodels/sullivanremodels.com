/** @type {import('next-sitemap').IConfig} */
const config = {
  siteUrl: "https://www.sullivanremodels.com",
  generateRobotsTxt: true,
  generateIndexSitemap: false,
  changefreq: "monthly",
};

module.exports = config;

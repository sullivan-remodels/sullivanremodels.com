/** @type {import('next').NextConfig} */
const nextConfig = {
  async redirects() {
    return [
      {
        source: "/photos",
        destination: "/remodeling/kitchen",
        permanent: true,
      },
      {
        source: "/painting",
        destination: "/remodeling",
        permanent: true,
      },
      {
        source: "/ourPromise",
        destination: "/contact",
        permanent: true,
      },
      {
        source: "/pressureWashing",
        destination: "/remodeling",
        permanent: true,
      },
    ];
  },
};

export default nextConfig;
